import { environment } from '@env/environment';

export const apiConfig = {
  calendar_api_url: {
    slots: environment.calendar_api_url.slots,
    empty: environment.calendar_api_url.empty,
    error: environment.calendar_api_url.error
  }
};
