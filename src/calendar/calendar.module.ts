import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CalendarContainer } from './container/calendar.container';
import { CalendarService } from './services/calendar.service';

@NgModule({
  declarations: [
    CalendarContainer
  ],
  imports: [
    CommonModule,
    HttpClientModule
  ],
  exports: [CalendarContainer],
  providers: [
    CalendarService
  ]
})
export class CalendarModule { }
