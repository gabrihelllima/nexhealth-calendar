import { Slot } from './slot.model';

export interface Calendar {
  day: string;
  slots: Slot[];
}
