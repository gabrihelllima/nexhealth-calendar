import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

import { apiConfig } from '@app/app.config';
import { Calendar } from '../models';

@Injectable()
export class CalendarService {
  private CALENDAR_SLOTS_URL: string = apiConfig.calendar_api_url.slots;

  constructor(private httpClient: HttpClient) {}

  getSlots(): Observable<any> {
    return this.httpClient.get(`${this.CALENDAR_SLOTS_URL}`).pipe(
      map((data: any) => {
        const slots = data[0]?.data[0]?.slots;

       return slots.map((item: any) => {
          const date = new Date(item['time']);
          const dateFormatted = new Intl.DateTimeFormat('en-US').format(date);
          const timeFormatted = date.toLocaleTimeString('en-US', { hour: '2-digit', minute: '2-digit' });

          return { 
            date: dateFormatted,
            slots: {
              time: timeFormatted,
              operatoryId: item['operatory_id' as 'operatoryId']
            }
          }
        }).reduce((obj: any, item: any) => {
          obj[item.date] = [...obj[item.date] || [], { ...item.slots }];

          return obj;
        }, {});
      }),
      catchError((error: any) => throwError(new Error(error)))
    );
  }

  getWeek(date: Date): any {
    return [
      date.getTime(), 
      ...Array.from(Array(4)).map(() => date.setDate(date.getDate() + 1))
    ];
  }

  getFullCalendar(data: any): Calendar[] {
    const week = this.getWeek(new Date(data[0][0]));
    const mapSlots = (day: string) => data.filter((item: any) => item[0] === day).map((x: any) => x[1])

    return week.map((day: any) => {
      const dayFormatted = new Intl.DateTimeFormat('en-US').format(day);

      return {
        day: dayFormatted,
        slots: mapSlots(dayFormatted)[0] || []
      }
    });
  };
}
