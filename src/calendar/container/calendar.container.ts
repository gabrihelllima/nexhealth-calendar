import { Component, OnInit } from '@angular/core';
import { Calendar, Slot } from '../models';
import { CalendarService } from '../services/calendar.service';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.container.html',
  styleUrls: ['./calendar.container.scss']
})
export class CalendarContainer implements OnInit {
  public calendar!: Calendar[];
  public toggle = false;
  public slotsShowLimit = 5;

  constructor(private calendarService: CalendarService) {}

  ngOnInit(): void {
    this.calendarService.getSlots().subscribe((data) => {
      data = Object.entries(data);

      console.log(this.calendarService.getFullCalendar(data));
      
      this.calendar = this.calendarService.getFullCalendar(data);
    });
  }

  showMoreItems(slot: any){
    console.log(slot);
    slot.slots.slice(0, 2);
    
    this.slotsShowLimit = Number(this.slotsShowLimit) + 3;
   }

   showLessItems(slot: any) {
    console.log(slot);
    slot.slots.slice(0, 2);
    //  this.slotsShowLimit = Number(this.slotsShowLimit) - 3;
   }
}
